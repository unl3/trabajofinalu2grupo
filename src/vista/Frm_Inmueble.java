/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Inmueble.InmuebleController;
import javax.swing.JOptionPane;
import vista.modeloTablas.ModeloTablaBienes;
import vista.modeloTablas.ModeloTablaBienesA;

/**
 *
 * @author jona
 */
public class Frm_Inmueble extends javax.swing.JDialog {

     private ModeloTablaBienesA mc = new ModeloTablaBienesA();
     private InmuebleController ic = new InmuebleController();

     boolean estado = false;
     private int accionTabla = 0;
     private Integer idBuscar;
     private int pos = -1;
     private int fila = -1;

     /**
      * Creates new form Frm_Inmueble
      */
     public Frm_Inmueble(java.awt.Frame parent, boolean modal) {
          super(parent, modal);
          initComponents();
          CargarTabla();
     }
     
      public void CargarTabla() {
       mc.setLista(ic.listado());
        tbl_bienes.setModel(mc);
        tbl_bienes.updateUI();
    }
      
       public void Limpiar() {
        txtdescrp.setText("");
        txtregistro.setText("");
        txtubi.setText("");
        txtdescrp.setText("");
    }
       
           private void validar(){
         txtregistro.getText().trim().isEmpty(); txtubi.getText().trim().isEmpty() ; txtvalor.getText().trim().isEmpty() ; txtdescrp.getText().trim().isEmpty();
        
    }

    public void guardar() throws ExceptionInInitializerError{
      
            ic.getInmueble().setId(id_inmueble);
            ic.getInmueble().setNro_Registro(txtregistro.getText());
            ic.getInmueble().setUbicacion(txtubi.getText());
            ic.getInmueble().setValor(txtvalor.getText());
            ic.getInmueble().setClasificacion(cbxclasif.getSelectedItem().toString());
            ic.getInmueble().setServicios(cbxservici.getSelectedItem().toString());
            ic.getInmueble().setTipo(cbxtipo.getSelectedItem().toString());
            ic.getInmueble().setDescripcion(txtdescrp.getText());
//            if (ic.getInmueble().getId() != null) {
//                if (ic.modificar(fila)) {
//                    JOptionPane.showMessageDialog(null, "Se ha modificado correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
//                    // limpiar();
//                    CargarTabla();
//                } else {
//                    JOptionPane.showMessageDialog(null, "No se modifico", "Error", JOptionPane.ERROR_MESSAGE);
//                }
//            } else {
//                ic.getInmueble().setNro_Registro(txtregistro.getText());
//                ic.getInmueble().setUbicacion(txtubi.getText());
//                ic.getInmueble().setValor(txtvalor.getText());
//                ic.getInmueble().setClasificacion(cbxclasif.getSelectedItem().toString());
//                ic.getInmueble().setServicios(cbxservici.getSelectedItem().toString());
//                ic.getInmueble().setTipo(cbxtipo.getSelectedItem().toString());
//                ic.getInmueble().setDescripcion(txtdescrp.getText());
                ic.guardar();
                    JOptionPane.showMessageDialog(null, "Se ha guardadao correctamente", "OK", JOptionPane.INFORMATION_MESSAGE);
                    CargarTabla();
                    Limpiar();

            
        
    }
private void modificar(){
    
    if (seleccionar()) {
        
         ic.getInmueble().setId(id_inmueble);
        
             ic.getInmueble().setId(id_inmueble);
            ic.getInmueble().setNro_Registro(txtregistro.getText());
            ic.getInmueble().setUbicacion(txtubi.getText());
            ic.getInmueble().setValor(txtvalor.getText());
            ic.getInmueble().setClasificacion(cbxclasif.getSelectedItem().toString());
            ic.getInmueble().setServicios(cbxservici.getSelectedItem().toString());
            ic.getInmueble().setTipo(cbxtipo.getSelectedItem().toString());
            ic.getInmueble().setDescripcion(txtdescrp.getText());
            ic.modificar(fila);
            
            System.out.println("Modificado");
            CargarTabla();
        
        
    }

}
    private Boolean seleccionar() {

        fila = tbl_bienes.getSelectedRow();

        if (fila >= 0) {
            System.out.println("ok");
            ic.setAuto(ic.listado().obtenerDato(fila));
            //txtregistro.setText(ic.getInmueble().getNro_Registro());
           // cbxservici.setSelectedItem(ic.getInmueble().getServicios());
            // txtvalor.setText("asjkdsakjjdksa");
         //   BtnGuardar.setText("MODIFICAR");
        } else {

            System.out.println("NO VALE ");
        }
        return true;

    }

     @SuppressWarnings("unchecked")
     // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
     private void initComponents() {

          jPanel1 = new javax.swing.JPanel();
          jScrollPane1 = new javax.swing.JScrollPane();
          tbl_bienes = new javax.swing.JTable();
          jPanel2 = new javax.swing.JPanel();
          jLabel2 = new javax.swing.JLabel();
          jLabel3 = new javax.swing.JLabel();
          jLabel4 = new javax.swing.JLabel();
          txtdescrp = new javax.swing.JTextField();
          txtregistro = new javax.swing.JTextField();
          txtubi = new javax.swing.JTextField();
          jLabel5 = new javax.swing.JLabel();
          jLabel6 = new javax.swing.JLabel();
          jLabel7 = new javax.swing.JLabel();
          cbxclasif = new javax.swing.JComboBox<>();
          cbxservici = new javax.swing.JComboBox<>();
          cbxtipo = new javax.swing.JComboBox<>();
          jLabel8 = new javax.swing.JLabel();
          txtvalor = new javax.swing.JTextField();
          BtnCancel = new javax.swing.JButton();
          BtnGuardar = new javax.swing.JButton();
          BtnNuevo = new javax.swing.JButton();

          setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
          getContentPane().setLayout(null);

          jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
          jPanel1.setLayout(null);

          tbl_bienes.setModel(new javax.swing.table.DefaultTableModel(
               new Object [][] {
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null}
               },
               new String [] {
                    "Title 1", "Title 2", "Title 3", "Title 4"
               }
          ));
          jScrollPane1.setViewportView(tbl_bienes);

          jPanel1.add(jScrollPane1);
          jScrollPane1.setBounds(20, 240, 660, 190);

          jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
          jPanel2.setLayout(null);

          jLabel2.setText("Descripcion");
          jPanel2.add(jLabel2);
          jLabel2.setBounds(20, 100, 90, 17);

          jLabel3.setText("Tipo");
          jPanel2.add(jLabel3);
          jLabel3.setBounds(290, 90, 90, 17);

          jLabel4.setText("Ubicacion");
          jPanel2.add(jLabel4);
          jLabel4.setBounds(20, 60, 90, 17);
          jPanel2.add(txtdescrp);
          txtdescrp.setBounds(110, 93, 130, 40);
          jPanel2.add(txtregistro);
          txtregistro.setBounds(110, 30, 130, 23);
          jPanel2.add(txtubi);
          txtubi.setBounds(110, 60, 130, 23);

          jLabel5.setText("Nro Registro");
          jPanel2.add(jLabel5);
          jLabel5.setBounds(20, 30, 90, 17);

          jLabel6.setText("Clacificacion");
          jPanel2.add(jLabel6);
          jLabel6.setBounds(290, 30, 90, 17);

          jLabel7.setText("Servicios");
          jPanel2.add(jLabel7);
          jLabel7.setBounds(290, 60, 90, 17);

          cbxclasif.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "RURAL", "URBANO" }));
          jPanel2.add(cbxclasif);
          cbxclasif.setBounds(390, 30, 120, 23);

          cbxservici.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "LUZ - AGUA", "LUZ - AGUA - ALCANTARILLADO", "TODOS" }));
          jPanel2.add(cbxservici);
          cbxservici.setBounds(390, 60, 120, 23);

          cbxtipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
          jPanel2.add(cbxtipo);
          cbxtipo.setBounds(390, 90, 120, 23);

          jLabel8.setText("Valor");
          jPanel2.add(jLabel8);
          jLabel8.setBounds(290, 120, 90, 17);
          jPanel2.add(txtvalor);
          txtvalor.setBounds(390, 120, 120, 23);

          BtnCancel.setText("Cancelar");
          jPanel2.add(BtnCancel);
          BtnCancel.setBounds(554, 70, 100, 23);

          BtnGuardar.setText("Guardar");
          BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    BtnGuardarActionPerformed(evt);
               }
          });
          jPanel2.add(BtnGuardar);
          BtnGuardar.setBounds(552, 30, 100, 23);

          BtnNuevo.setText("Nuevo");
          jPanel2.add(BtnNuevo);
          BtnNuevo.setBounds(554, 110, 100, 23);

          jPanel1.add(jPanel2);
          jPanel2.setBounds(10, 30, 680, 170);

          getContentPane().add(jPanel1);
          jPanel1.setBounds(10, 10, 700, 500);

          setSize(new java.awt.Dimension(729, 552));
          setLocationRelativeTo(null);
     }// </editor-fold>//GEN-END:initComponents

     private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
         guardar();
     }//GEN-LAST:event_BtnGuardarActionPerformed

     /**
      * @param args the command line arguments
      */
     public static void main(String args[]) {
          /* Set the Nimbus look and feel */
          //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
          /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
           */
          try {
               for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                         javax.swing.UIManager.setLookAndFeel(info.getClassName());
                         break;
                    }
               }
          } catch (ClassNotFoundException ex) {
               java.util.logging.Logger.getLogger(Frm_Inmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          } catch (InstantiationException ex) {
               java.util.logging.Logger.getLogger(Frm_Inmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          } catch (IllegalAccessException ex) {
               java.util.logging.Logger.getLogger(Frm_Inmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          } catch (javax.swing.UnsupportedLookAndFeelException ex) {
               java.util.logging.Logger.getLogger(Frm_Inmueble.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
          }
          //</editor-fold>

          /* Create and display the dialog */
          java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                    Frm_Inmueble dialog = new Frm_Inmueble(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                         @Override
                         public void windowClosing(java.awt.event.WindowEvent e) {
                              System.exit(0);
                         }
                    });
                    dialog.setVisible(true);
               }
          });
     }

     // Variables declaration - do not modify//GEN-BEGIN:variables
     private javax.swing.JButton BtnCancel;
     private javax.swing.JButton BtnGuardar;
     private javax.swing.JButton BtnNuevo;
     private javax.swing.JComboBox<String> cbxclasif;
     private javax.swing.JComboBox<String> cbxservici;
     private javax.swing.JComboBox<String> cbxtipo;
     private javax.swing.JLabel jLabel2;
     private javax.swing.JLabel jLabel3;
     private javax.swing.JLabel jLabel4;
     private javax.swing.JLabel jLabel5;
     private javax.swing.JLabel jLabel6;
     private javax.swing.JLabel jLabel7;
     private javax.swing.JLabel jLabel8;
     private javax.swing.JPanel jPanel1;
     private javax.swing.JPanel jPanel2;
     private javax.swing.JScrollPane jScrollPane1;
     private javax.swing.JTable tbl_bienes;
     private javax.swing.JTextField txtdescrp;
     private javax.swing.JTextField txtregistro;
     private javax.swing.JTextField txtubi;
     private javax.swing.JTextField txtvalor;
     // End of variables declaration//GEN-END:variables
}
